#!/usr/bin/env bash

ip -json link | jq -c -M '.|map(select(.link_type == "ether"))|map(.ifname)'
