import logging
from .main import main


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main()
