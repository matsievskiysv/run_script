#!/usr/bin/env bash

set -e

for iface in $(ip -br link show group 42 | cut -f1 -d' ')
do
    echo clear "$iface"
    sudo ip link set dev "$iface" group 0
    find /etc/systemd/network/ -iname "*${iface}*" -exec sudo sed -i 's/^Group=.*/Group=1/g' {} \;
done

for iface in "$@"
do
    echo set "$iface"
    sudo ip link set group 42 "$iface"
    find /etc/systemd/network/ -iname "*${iface}*" -exec sudo sed -i 's/^Group=.*/Group=42/g' {} \;
done
