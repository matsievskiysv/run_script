import { io } from "/static/socket.io.esm.min.js";
/* import { io } from "https://cdn.socket.io/4.4.1/socket.io.esm.min.js"; */

const socket = io();

socket.on("connect", () => {
	// request server status on startup
	socket.emit("status_request");
});

function self_update(config) {
    	    const items = document.getElementsByClassName("script_list_items");
	    Array.from(items).forEach((item) => {
		if (item.querySelector("a").href.split("/").slice(-1)[0] == config["script"]) {
		    if (config["status"] == "WAITING") {
			item.classList.remove("selected_item");
		    } else {
			item.classList.add("selected_item");
		    }
		} else {
		    item.classList.remove("selected_item");
		}
	    });
}

// status update on new connection
socket.on("status_update", (msg) => {
	const server_status = document.getElementById("server_status");
	let status = "INVALID";
	try {
	    const message = JSON.parse(msg);
	    status = message["state"];
	    self_update(message);
	} catch (e) {
	    console.log(e.message);
	}
	server_status.textContent = status;
});
