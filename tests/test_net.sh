#!/usr/bin/env bash

RED='\E[1;31m'
GREEN='\E[1;32m'
YELLOW='\E[1;33m'
NC='\033[1m\033[0m'

NETGROUP=${NETGROUP:-42}
IP1=${IP1:-10.11.12.13}
IPMASK1=${IPMASK1:-24}
IP2=${IP2:-10.11.12.14}
IPMASK2=${IPMASK2:-24}
NSNAME=${NSNAME:-testns}

trap cleanup 1 2 3 6

# Echo error string
echo_error() {
    echo -e "${RED}$*${NC}" 1>&2
}

# Echo warning string
echo_warn() {
    echo -e "${YELLOW}$*${NC}" 1>&2
}

# Echo info string
echo_info() {
    echo -e "${GREEN}$*${NC}"
}

# Create namespace
# $1: name
create_namespace() {
    local name=$1
    if sudo ip -br netns list | grep -q "$name"; then
        echo_warn namespace "$name" already exists
    else
        sudo ip netns add "$name"
        sudo ip netns exec "$name" sudo ip link set dev lo up
    fi
}

# Remove all namespaces
delete_namespaces() {
    for name in $(sudo ip netns list | awk '{print($1)}'); do
        sudo ip netns delete "$name"
    done
}

# Remove namespace
# $1: name
delete_namespace() {
    local name=$1
    if sudo ip -br netns list | grep -q "$name"; then
        sudo ip netns delete "$name"
    else
        echo_warn namespace "$name" already does not exist
    fi
}

# Apply function to combinations of list arguments
# $1: function
# $2-n: items
do_over_combinations() {
    local func=$1
    shift
    local ifs=("$@")
    for x in $(seq 0 $((${#ifs[@]}-2))); do
        for y in $(seq $((x+1)) $((${#ifs[@]}-1))); do
            $func "${ifs[x]}" "${ifs[y]}"
        done
    done
}


# Put interface to namespace
# $1: interface
# $2: namespace name
put_to_namespace() {
    local ifs1=$1
    local name="$2"
    sudo ip link set dev "$ifs1" netns "$name"
    sudo ip netns exec "$name" ip link set dev "$ifs1" up
}

# Set interface address
# $1: interface
# $2: address
# $3: namespace name
set_address() {
    local ifs1=$1
    local address=$2
    local name=$3

    if [[ $3 == "" ]]; then
        sudo ip address flush dev "$ifs1"
        sudo ip address add dev "$ifs1" "$address"
    else
        sudo ip netns exec "$name" ip address flush dev "$ifs1"
        sudo ip netns exec "$name" ip address add dev "$ifs1" "$address"
    fi
}

# Put interface from namespace to default
# $1: interface
# $2: namespace name
return_from_namespace() {
    local ifs1=$1
    local name=$2
    sudo ip netns exec "$name" ip link set dev "$ifs1" netns 1
    sleep 0.1
    sudo ip link set dev "$ifs1" group "${NETGROUP}"
    sudo ip link set dev "$ifs1" up
}

# Ping two interfaces
# $1: interface 1
# $2: interface 2
ping_interfaces() {
    local ifs1=$1
    local ifs2=$2
    if [ -z "$ifs2" ]; then
	echo_error 'Missing interface'
	return
    fi
    create_namespace "$NSNAME"
    put_to_namespace "$ifs2" "$NSNAME"
    set_address "$ifs1" "${IP1}/${IPMASK1}"
    set_address "$ifs2" "${IP2}/${IPMASK2}" "$NSNAME"
    printf '%s <-> %s\t: ' "$ifs1" "$ifs2"
    if ping -q -c 10 -I "$ifs1" "$IP2" > /dev/null; then
	echo_info ok
    else
	echo_error fail
    fi
    return_from_namespace "$ifs2" "$NSNAME"
    delete_namespace "$NSNAME"
}

# iPerf two interfaces
# $1: interface 1
# $2: interface 2
iperf_interfaces() {
    local ifs1=$1
    local ifs2=$2
    local tempfile
    if [ -z "$ifs2" ]; then
	echo_error 'Missing interface'
	return
    fi
    tempfile=$(mktemp)
    chmod 777 "$tempfile"
    create_namespace "$NSNAME"
    put_to_namespace "$ifs2" "$NSNAME"
    set_address "$ifs1" "${IP1}/${IPMASK1}"
    set_address "$ifs2" "${IP2}/${IPMASK2}" "$NSNAME"
    printf '%s <-> %s\t: ' "$ifs1" "$ifs2"
    if ping -q -c 10 -I "$ifs1" "$IP2" > /dev/null; then
	iperf3 \
	    --server \
	    --one-off \
	    --idle-timeout 30 \
	    --bind "$IP1" \
	    --bind-dev "$ifs1" > /dev/null 2>&1 &
	sudo ip netns exec "$NSNAME" iperf3 \
	      --json \
	      --omit 2 \
	      --connect-timeout 10 \
	      --parallel "$(nproc)" \
	      -c "$IP1" \
	      --bind "$IP2" \
	      --bind-dev "$ifs2" > "$tempfile"
	if [[ $(jq .error < "$tempfile") == null ]]; then
	    local send recieve
            send=$(jq '.end.sum_sent.bits_per_second/1024/1024' < "$tempfile")
	    recieve=$(jq '.end.sum_sent.bits_per_second/1024/1024' < "$tempfile")
            # shellcheck disable=SC2046
	    echo_info $(printf 'send %0.0f[Mb/s] recieve %0.0f[Mb/s]' "$send" "$recieve")
	else
	    echo_error fail
	fi
    else
	echo_error fail
    fi
    return_from_namespace "$ifs2" "$NSNAME"
    delete_namespace "$NSNAME"
    rm -rf "$tempfile"
}

# Cleanup function
cleanup() {
    delete_namespaces
}

# Get ether interfaces and mark if present in specified group
# $1: group to mark as true
interface_group_list_marked() {
    local grp=${1:-default}
    sudo ip --json link list | jq -r '.|map(select(.link_type=="ether"))|map(.group == "'"${grp}"'", .ifname)[]'
}

# Get ether interfaces in specified group
# $1: group to select
interface_group_list() {
    local grp=${1:-default}
    sudo ip --json link list | jq -r '.|map(select(.link_type=="ether" and .group == "'"${grp}"'"))|map(.ifname)[]'
}

# Test ping
# $1: network interfaces list
test_ping() {
    set -e
    echo_warn Ping
    # shellcheck disable=SC1009,SC2068
    do_over_combinations ping_interfaces $@

}

# Test iperf
# $1: network interfaces list
test_iperf() {
    set -e
    echo_warn iPerf
    # shellcheck disable=SC1009,SC2068
    do_over_combinations iperf_interfaces $@

}

# Main function
# $1: network interfaces list
main() {
    set -e
    # shellcheck disable=SC1009,SC2068
    test_ping $@
    # shellcheck disable=SC1009,SC2068
    test_iperf $@
}

case "$1" in
    ping)
        test_ping $(interface_group_list ${NETGROUP})
        ;;
    iperf)
        test_iperf $(interface_group_list ${NETGROUP})
        ;;
    *)
        echo_error 'Usage: ./test_net.sh ping|iperf'
        ;;
esac


# if [ -z "$DISPLAY" ]; then
    # CLI mode
    # case "$1" in
    #     interfaces)
    #         shift
    #         if [ -z "$*" ]; then
    #     	echo_error 'Usage: ./test_net.sh interfaces <if1> <if2> <if3> ...'
    #         else
    #     	main "$*"
    #         fi
    #         ;;
    #     group)
    #         shift
    #         grp="$NETGROUP"
    #         if [ -n "$1" ]; then
    #     	grp="$1"
    #         fi
    #         ifs=($(interface_group_list "$grp"))
    #         if [ -z "$ifs" ]; then
    #             echo_error "No interfaces in group $grp"
    #         else
    #     	main "${ifs[@]}"
    #         fi
    #         ;;
    #     *)
    #         echo_error 'Usage: ./test_net.sh interfaces <if1> <if2> <if3> ...'
    #         echo_error '       ./test_net.sh group [<group_number>]'
    #         ;;
    # esac
# else
#     # GUI mode
#     ifs=($(zenity --title='Select interfaces' \
# 		  --text='Select interfaces for testing' \
# 		  --list --checklist --multiple \
# 		  --separator=' ' \
# 		  --column="Enable" --column="Interface" \
# 		  $(interface_group_list_marked ${NETGROUP})))
#     if [ -z "$*" ]; then
# 	echo_error 'No interfaces selected'
#     else
# 	main "$*"
#     fi
# fi
