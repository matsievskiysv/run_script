from datetime import datetime
from flask import Flask, render_template, send_from_directory
from flask_socketio import SocketIO, emit
from pathlib import Path
from sys import argv, exit
from typing import Optional
import logging
import threading

from .parser import load_list
from .runner import Runner


app = Flask(__name__)
socketio = SocketIO(app)
config = None  # type: Optional[dict]
runner = None  # type: Optional[Runner]


@app.route("/favicon.ico")
def favicon():
    return send_from_directory(Path(app.root_path) / "static",
                               "favicon.ico", mimetype="image/vnd.microsoft.icon")


@app.route("/", methods=["GET"])
def list_tests():
    return render_template("list.html", scriptlist=[(num, id, config["name"]) for num, (id, config)
                                                    in enumerate(config["scripts"].items())])


@app.route("/scripts/<script_id>", methods=["GET"])
def script_page(script_id: str):
    assert config is not None
    if script_id in config["scripts"].keys():
        return render_template("script.html",
                               script_id=script_id,
                               script=config["scripts"][script_id],
                               stagelist=enumerate(config["scripts"][script_id]["stages"]))
    else:
        return render_template("script_404.html")


@app.route("/scripts/<script_id>/logs", methods=["GET"])
def logs_page(script_id: str):
    assert config is not None
    if script_id in config["scripts"].keys():
        dir, file = runner.get_archive()
        if file is not None:
            time = datetime.now().isoformat().replace(":", ".")
            return send_from_directory(dir, file, mimetype="application/zip",
                                       download_name=f"logs-{script_id}-{time}.zip")
        else:
            return "zipping", 302
    else:
        return render_template("script_404.html")


@socketio.on('connection')
def connection_event():
    logging.info("new connection")


@socketio.event
def status_request():
    assert runner is not None
    logging.debug("status request")
    emit("status_update", runner.state())


@socketio.event
def start_script(script_name: str, position: int):
    assert runner is not None
    logging.debug(f"start script {script_name} at {position}")
    success, msg = runner.start_script(script_name, position)
    if not success:
        emit("server_message", msg)


@socketio.event
def stop_script(script_name: str):
    assert runner is not None
    logging.debug(f"stop script {script_name}")
    success, msg = runner.stop_script(script_name)
    if not success:
        emit("server_message", msg)


@socketio.event
def continue_script(script_name: str):
    assert runner is not None
    logging.debug(f"continue script {script_name}")
    success, msg = runner.continue_script(script_name)
    if not success:
        emit("server_message", msg)


@socketio.event
def cleanup_script(script_name: str):
    assert runner is not None
    logging.debug(f"cleanup script {script_name}")
    success, msg = runner.cleanup_script(script_name)
    if not success:
        emit("server_message", msg)


@socketio.event
def request_archive(script_name: str):
    assert runner is not None
    logging.debug(f"request archive of script {script_name}")
    success, msg = runner.request_archive(script_name)
    if not success:
        emit("server_message", msg)


@socketio.event
def append_input(script_name: str, input_data: str):
    assert runner is not None
    logging.debug(f"append input for script {script_name}")
    success, msg = runner.append_input(script_name, input_data)
    if not success:
        emit("server_message", msg)


def main():
    global config, runner
    if len(argv) != 2:
        exit("missing configuration file")
    config_file = Path(argv[1])
    if not config_file.is_file():
        exit(f"{config_file} is not a file")
    logging.info(f"using config file {config_file.resolve()}")
    config = load_list(config_file.resolve())  # this structure is read only from now on
    runner = Runner(socketio, config)
    runner_thread = threading.Thread(target=runner.run)
    runner_thread.start()
    socketio.run(app, host="0.0.0.0", port=5000, debug=True)
    runner.shutdown()
    runner_thread.join()
