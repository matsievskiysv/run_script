from datetime import datetime
from enum import Enum, auto
from flask_socketio import SocketIO
from pathlib import Path
from shutil import rmtree
from tempfile import mkdtemp
from typing import Tuple, Callable, Optional, Any
import json
import logging
import subprocess
import threading
import zipfile


class ServerState(Enum):
    WAITING = auto()        # ready to run
    RUNNING = auto()        # running
    STOPPING = auto()       # request to stop
    KILLING = auto()        # request to force stop
    ZIPPING = auto()        # zipping files
    SHUTDOWN = auto()       # stopping runner thread
    CLEANUP = auto()        # cleaning directory
    AWAIT_CLEANUP = auto()  # stopped and waiting for user to get work directory contents
    AWAIT_INPUT = auto()    # running and waiting for user input


class Locked():
    def __init__(self, obj):
        self.__lock = threading.Lock()
        self.__obj = obj

    def __enter__(self):
        self.__lock.acquire()
        return self.__obj

    def __exit__(self, exception_type, exception_value, exception_traceback):
        self.__lock.release()


class ThreadContext():

    def __init__(self):
        self.state = ServerState.WAITING    # type: ServerState # status of runner
        self.workdir = None                 # type: Optional[Path] # location of runner files
        self.script = ""                    # type: str # currently running process
        self.payload_tx = []                # type: list # optional state payload to transmit
        self.payload_rx = ""                # type: str # optional state payload to receive
        self.message = ""                   # type: str # server message
        self.position = 0                   # type: int # current script position
        self.out = None                     # type: Optional[_io.TextIOWrapper] # file for script output
        self.err = None                     # type: Optional[_io.TextIOWrapper] # file for script error
        self.timestamp = datetime.now()     # type: datetime.datetime # timestamp of currently running process
        self.process = None                 # type: Optional[subprocess.Popen] # currently running process


class Runner():

    TERMINATE_TIMEOUT = 10  # type: int
    POLL_TIME = 0.1  # type: float
    STDIN_EXTENSION = "in"  # type: str
    STDOUT_EXTENSION = "out"  # type: str
    STDERR_EXTENSION = "err"  # type: str
    RV_EXTENSION = "rv"  # type: str
    FILE_FORMAT = "{:02d}.{}"  # type: str

    def __init__(self, socketio: SocketIO, config: dict):
        self.STATE_TRANSITION = {
            ServerState.WAITING: self.state_waiting,
            ServerState.RUNNING: self.state_running,
            ServerState.STOPPING: self.state_stopping,
            ServerState.KILLING: self.state_killing,
            ServerState.ZIPPING: self.state_zipping,
            ServerState.CLEANUP: self.state_cleanup,
            ServerState.AWAIT_CLEANUP: self.state_await_cleanup,
            ServerState.AWAIT_INPUT: self.state_await_input,
        }

        self.STAGE_HANDLER = {
            "file": self.stage_file,
            "tail": self.stage_tail,
            "message": self.stage_message,
            "checkbox": self.stage_checkbox,
            "list": self.stage_list,
        }

        self.SUMMARY_HANDLER = {
            "return_code_check": self.summary_return_code_check,
        }

        self.__socketio = socketio
        self.__config = config
        self.__context = Locked(ThreadContext())
        self.__event = threading.Event()  # wakeup event for runner
        self.__event.clear()
        self.__signal_sent = False  # type: bool # for edge event detection in stop state

    def start_script(self, script: str, position: int) -> Tuple[bool, str]:
        if script not in self.__config["scripts"].keys():
            logging.warn("script not found")
            return False, "Script not found"
        if position < 0 or position > len(self.__config["scripts"][script]["stages"]):
            return False, "Position out of range"

        logging.debug("start script")
        with self.__context as context:
            if context.workdir is not None:
                logging.error("workdir exists at start or execution")
                return False, "Work directory exists"
            context.state = ServerState.RUNNING
            context.script = script
            context.position = position
            context.workdir = Path(mkdtemp(prefix=f"test_studio-{script}-"))
        self.wake_up()
        return True, ""

    def stop_script(self, script: str) -> Tuple[bool, str]:
        if script not in self.__config["scripts"].keys():
            logging.warn("script not found")
            return False, "Script not found"

        logging.debug("stop script")
        with self.__context as context:
            if script == context.script:
                context.state = ServerState.STOPPING
            else:
                return False, "Cannot stop other script"
        self.wake_up()
        return True, "ok"

    def continue_script(self, script: str) -> Tuple[bool, str]:
        if script not in self.__config["scripts"].keys():
            logging.warn("script not found")
            return False, "Script not found"

        logging.debug("continue script")
        self.wake_up()
        return True, "ok"

    def cleanup_script(self, script: str) -> Tuple[bool, str]:
        if script not in self.__config["scripts"].keys():
            logging.warn("script not found")
            return False, "Script not found"

        logging.debug("cleanup script")
        with self.__context as context:
            if script == context.script:
                context.state = ServerState.STOPPING
            else:
                return False, "Cannot stop other script"
            context.state = ServerState.CLEANUP
        self.wake_up()
        return True, "ok"

    def request_archive(self, script: str) -> Tuple[bool, str]:
        if script not in self.__config["scripts"].keys():
            logging.warn("script not found")
            return False, "Script not found"
        with self.__context as context:
            status = context.state
        assert context.workdir is not None
        if status == ServerState.AWAIT_CLEANUP:
            archive = context.workdir / "logs.zip"
            if archive.is_file():
                return True, "Done"
            else:
                with self.__context as context:
                    context.state = ServerState.ZIPPING
                self.wake_up()
        return True, "Ok"

    def request_archive(self, script: str) -> Tuple[bool, str]:
        if script not in self.__config["scripts"].keys():
            logging.warn("script not found")
            return False, "Script not found"
        with self.__context as context:
            status = context.state
        assert context.workdir is not None
        if status == ServerState.AWAIT_CLEANUP:
            archive = context.workdir / "logs.zip"
            if archive.is_file():
                self.__socketio.emit("status_update", self.state(), broadcast=True)
                return True, "Done"
            else:
                with self.__context as context:
                    context.state = ServerState.ZIPPING
                self.wake_up()
        return True, "Ok"


    def get_archive(self) -> Tuple[str, Optional[str]]:
        with self.__context as context:
            status = context.state
        assert context.workdir is not None
        if status == ServerState.AWAIT_CLEANUP:
            archive = context.workdir / "logs.zip"
            if archive.is_file():
                return str(context.workdir), "logs.zip"
            else:
                with self.__context as context:
                    context.state = ServerState.ZIPPING
                self.wake_up()
                return str(context.workdir), None
        else:
            return str(context.workdir), None

    def state(self) -> str:
        with self.__context as context:
            return json.dumps({
                "state": context.state.name,
                "script": context.script,
                "position": context.position,
                "payload": context.payload_tx,
                "message": context.message,
                "timestamp": context.timestamp.isoformat(),
            })

    def wake_up(self):
        with self.__context:
            self.__event.set()

    def stop(self):
        with self.__context as context:
            context.state = ServerState.STOPPING
        self.wake_up()

    def shutdown(self):
        with self.__context as context:
            context.state = ServerState.SHUTDOWN
        self.wake_up()

    def append_input(self, script: str, input_string: str) -> Tuple[bool, str]:
        if script not in self.__config["scripts"].keys():
            logging.warn("script not found")
            return False, "Script not found"
        with self.__context as context:
            if context.state == ServerState.AWAIT_INPUT:
                logging.debug(f"append input: {input_string}")
                context.payload_rx += " "
                context.payload_rx += input_string
                context.payload_rx.strip()
            else:
                return False, "Wrong state"
        return True, ""

    def state_waiting(self) -> bool:
        logging.debug("handle waiting")
        self.__signal_sent = False
        with self.__context as context:
            context.script = ""
            context.message = ""
        self.__event.wait()
        with self.__context:
            self.__event.clear()
        logging.debug("woke up")
        return True

    def state_await_cleanup(self) -> bool:
        logging.debug("handle await cleanup")
        with self.__context as context:
            summary_handler = self.SUMMARY_HANDLER[self.__config["summary"]]
            context.message = summary_handler(context)
        self.__socketio.emit("status_update", self.state(), broadcast=True)
        self.__event.wait()
        with self.__context:
            self.__event.clear()
        logging.debug("woke up")
        return True

    def state_await_input(self) -> bool:
        logging.debug("handle await input")
        self.__event.wait()
        with self.__context as context:
            self.__event.clear()
            if context.state != ServerState.STOPPING:
                context.state = ServerState.RUNNING
            context.position += 1
            context.payload_tx = ""
        logging.debug("woke up")
        return True

    def state_killing(self) -> bool:
        logging.debug("handle killing")
        with self.__context as context:
            if context.process is not None:
                context.process.kill()
                context.out.close()
                context.err.close()
                context.process = None
            context.state = ServerState.AWAIT_CLEANUP
        return True

    def state_zipping(self) -> bool:
        logging.debug("handle zipping")
        with self.__context as context:
            self.__event.clear()
            workdir = context.workdir
        files = list(workdir.glob("*"))
        with zipfile.ZipFile(workdir / "logs.zip", 'w', zipfile.ZIP_DEFLATED) as zipf:
            for file in files:
                zipf.write(file, arcname=file.name)
        with self.__context as context:
            context.state = ServerState.AWAIT_CLEANUP
        return True

    def state_cleanup(self) -> bool:
        logging.debug("handle cleanup")
        with self.__context as context:
            self.__event.clear()
            logging.debug(f"remove directory {context.workdir}")
            rmtree(context.workdir)
            context.workdir = None
            context.state = ServerState.WAITING
        return True

    def state_stopping(self) -> bool:
        logging.debug("handle stopping")
        with self.__context as context:
            if context.process is None:
                # no subprocess
                context.state = ServerState.AWAIT_CLEANUP
                return True
            else:
                # subprocess is running
                if self.__signal_sent:
                    if context.process.poll():
                        file_name = self.FILE_FORMAT.format(context.position, self.RV_EXTENSION)
                        with open(context.workdir / file_name, "w") as file:
                            file.write(str(context.process.returncode))
                        context.process = None
                        context.position = 0
                        context.out.close()
                        context.err.close()
                        context.state = ServerState.AWAIT_CLEANUP
                        return True
                    else:
                        timedelta = datetime.now() - context.timestamp
                        if timedelta.seconds >= self.TERMINATE_TIMEOUT:
                            context.state = ServerState.KILLING
                            return True
                        else:
                            self.__event.wait(self.POLL_TIME)
                            self.__event.clear()
                            return False
                else:
                    context.process.terminate()
                    self.__signal_sent = True
                    return True

    def state_running(self) -> bool:
        logging.debug("handle running")
        with self.__context as context:
            if context.position >= len(self.__config["scripts"][context.script]["stages"]):
                context.state = ServerState.STOPPING
                return True
            else:
                logging.debug(f"running at position {context.position}")
                stage = self.__config["scripts"][context.script]["stages"][context.position]
                handle = self.STAGE_HANDLER.get(stage["type"], None)
                if handle is None:
                    logging.error(f"unknown stage type {stage['type']}")
                    context.state = ServerState.STOPPING
                    return True
                # handles are called with locked context
                return handle(context, stage)

    def stage_tail(self, context: ThreadContext, stage: dict) -> bool:
        suffix = self.STDOUT_EXTENSION if stage["stream"] == "output" else self.STDERR_EXTENSION
        file_name = self.FILE_FORMAT.format(stage["index"], suffix)
        assert context.workdir is not None
        file = Path(context.workdir / file_name)
        logging.debug(f"trying to read file {file}")
        if file.is_file():
            with open(file, "r") as f:
                logging.debug(f"reading file {file}")
                context.payload_tx = ["".join(f.readlines()[-stage["count"]:])]
                context.state = ServerState.AWAIT_INPUT
        else:
            logging.warn(f"cannot file file {file}")
            context.position += 1
            context.payload_tx = ""
        return True

    def stage_file(self, context: ThreadContext, stage: dict) -> bool:
        if context.process is None:
            # start process
            file_name_in = self.FILE_FORMAT.format(context.position, self.STDIN_EXTENSION)
            file_name_out = self.FILE_FORMAT.format(context.position, self.STDOUT_EXTENSION)
            file_name_err = self.FILE_FORMAT.format(context.position, self.STDERR_EXTENSION)
            assert context.workdir is not None
            context.out = open(context.workdir / file_name_out, "w")
            context.err = open(context.workdir / file_name_err, "w")
            script = str(Path(self.__config["files"][stage["ref"]]["file"]).resolve())
            cmd = [script] + stage.get("extra_args", []) + context.payload_rx.split()
            context.process = subprocess.Popen(cmd,
                                               env={
                                                   "PATH": ":".join([str(p) for p in
                                                                     self.__config["environment"]["path"]]),
                                               },
                                               stdout=context.out,
                                               stderr=context.err,
                                               cwd=context.workdir)
            with open(context.workdir / file_name_in, "w") as file:
                file.write(" ".join(cmd))
            context.payload_rx = ""  # flush function arguments
            context.timestamp = datetime.now()
            logging.debug(f"executing script {script} at {context.timestamp}")
            return True
        else:
            # poll process
            if context.process.poll() is None:
                self.__event.wait(self.POLL_TIME)
                self.__event.clear()
                return False
            else:
                logging.debug("process stopped")
                file_name = self.FILE_FORMAT.format(context.position, self.RV_EXTENSION)
                assert context.workdir is not None
                return_code = context.process.returncode
                with open(context.workdir / file_name, "w") as file:
                    file.write(str(return_code))
                    context.process = None
                    assert context.out is not None
                    context.out.close()
                    context.out = None
                    assert context.err is not None
                    context.err.close()
                    context.err = None
                    context.position += 1
                    context.payload_tx = ""
                if stage.get("stop_on_error", True):
                    success_codes = stage.get("success_codes", [0])
                    if return_code not in success_codes:
                        context.state = ServerState.STOPPING
                return True

    def get_field_value(self, context: ThreadContext, field: Any, fallback_value: Any):
        if isinstance(field, dict):
            field_type = field.get("type", None)
            if field_type == "from_file":
                try:
                    with open(context.workdir / field["name"], "r") as file:
                        return json.load(file)
                except Exception:
                    return fallback_value
            elif field_type == "from_stream_file":
                try:
                    suffix = self.STDOUT_EXTENSION if field["stream"] == "output" else self.STDERR_EXTENSION
                    file_name = self.FILE_FORMAT.format(field["index"], suffix)
                    with open(context.workdir / file_name, "r") as file:
                        return json.load(file)
                except Exception:
                    return fallback_value
        return field

    def stage_message(self, context: ThreadContext, stage: dict) -> bool:
        context.payload_tx = [self.get_field_value(context, stage["text"], "empty message")]
        context.state = ServerState.AWAIT_INPUT
        return True

    def stage_checkbox(self, context: ThreadContext, stage: dict) -> bool:
        context.payload_tx = [self.get_field_value(context, stage["text"], "empty description"),
                              self.get_field_value(context, stage.get("value", False), False)]
        context.state = ServerState.AWAIT_INPUT
        return True

    def stage_list(self, context: ThreadContext, stage: dict) -> bool:
        context.payload_tx = [self.get_field_value(context, stage["text"], "empty description"),
                              self.get_field_value(context, stage.get("multiple", False), False),
                              self.get_field_value(context, stage["items"], [])]
        context.state = ServerState.AWAIT_INPUT
        return True

    def summary_return_code_check(self, context: ThreadContext) -> str:
        stages = self.__config["scripts"][context.script]["stages"]
        for index, stage in enumerate(stages):
            if stage["type"] == "file":
                rv_file =  context.workdir / self.FILE_FORMAT.format(index, self.RV_EXTENSION)
                if not rv_file.exists():
                    return "Some scripts were not executed"
                with open(rv_file, "r") as file:
                    output = file.read()
                try:
                    return_code = int(output)
                except ValueError:
                    return "Some scripts did not return"
                success_codes = stage.get("success_codes", [0])
                if return_code not in success_codes:
                    return "Some scripts failed"
        return "Success"

    def run(self):
        """Runner thread loop"""
        while True:
            with self.__context as context:
                logging.debug(f"new loop: {context.state}")
                handle = self.STATE_TRANSITION.get(context.state, None)
            if handle is None:
                # shutdown state
                self.state_killing()
                break
            if handle():
                self.__socketio.emit("status_update", self.state(), broadcast=True)
