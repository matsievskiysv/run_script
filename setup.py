from setuptools import setup, find_packages
from codecs import open
from pathlib import Path
import power_control.version


here = Path(__file__).parent.absolute()

# Get the long description from the README file
with open(here / "README.md", encoding="utf-8") as f:
    long_description = f.read()

setup(
    name="power_control",
    version=power_control.version.version,
    author="S.V. Matsievskiy",
    author_email="matsievskiysv@gmail.com",
    maintainer="S.V. Matsievskiy",
    maintainer_email="matsievskiysv@gmail.com",
    url="https://gitlab.com/matsievskiysv/power_control",
    description="Control panel for voltage sources",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license="GPLv3+",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)"
        "Operating System :: OS Independent",
        "Framework :: Flask",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: Scientific/Engineering",
    ],
    keywords="voltage source control",
    packages=find_packages(
        exclude=["*.tests", "*.tests.*", "tests.*", "tests"]
    ),
    include_package_data=True,
    install_requires=["pyserial>=3.5", "flask>=2.1.2"],
    extras_require={"dev": ["check-manifest"]},
    entry_points={
        'console_scripts': ['power_control = power_control.power:main']
    }
)
