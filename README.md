
```mermaid
flowchart TB
   WAITING -->|start button| RUNNING
   RUNNING -->|stop button| STOPPING
   AWAIT_INPUT -->|stop button| STOPPING
   AWAIT_INPUT -->|user input| RUNNING
   AWAIT_CLEANUP -->|clear button| CLEANUP
   CLEANUP -->|cleanup| WAITING
   AWAIT_CLEANUP -->|download logs button| ZIPPING
   ZIPPING -->|zipping complete| AWAIT_CLEANUP
   RUNNING -->|script completion| STOPPING
   RUNNING -->|input request| AWAIT_INPUT
   STOPPING -->|not timed out| AWAIT_CLEANUP
   STOPPING -->|timed out| KILLING
   KILLING --> AWAIT_CLEANUP
```
