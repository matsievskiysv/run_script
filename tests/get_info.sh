#!/usr/bin/env bash

set -e

sudo ip address flush dev "$1"
sudo ip address add dev "$1" 192.168.127.252/24

curl --connect-timeout 5 --interface "$1" -s --user admin:password -H "Content-Type: application/json" -d \
'{"id":"1","method":"systemUtility.status.boardinfo.get","params":[]}' \
192.168.127.253/json_rpc > /dev/null
curl --connect-timeout 5 --interface "$1" -s --user admin:password -H "Content-Type: application/json" -d \
'{"id":"1","method":"systemUtility.status.boardinfo.get","params":[]}' \
192.168.127.253/json_rpc | jq -M -C '.result' | sed 's/[{},"]//g'

sudo ip address flush dev "$1"
