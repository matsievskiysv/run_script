from jsonschema import validate
from pathlib import Path
from yaml import safe_load

from .schemas import LIST_SCHEMA


def load_list(path: Path) -> dict:
    with open(path) as f:
        obj = safe_load(f)
    validate(obj, LIST_SCHEMA)
    for ref, file in obj["files"].items():
        file_path = path.parent / Path(file["file"])
        if not file_path.exists():
            raise FileNotFoundError(file_path.name)
        obj["files"][ref] = file
    for _, script in obj["scripts"].items():
        for stage in script["stages"]:
            if stage["type"] == "file":
                if stage["ref"] not in obj["files"].keys():
                    raise AttributeError(f'{stage["ref"]} not found in files')
    for i, _ in enumerate(obj["environment"]["path"]):
        dir_path = path.parent / Path(obj["environment"]["path"][i])
        obj["environment"]["path"][i] = dir_path
    return obj
