#!/usr/bin/env bash

RED='\E[1;31m'
GREEN='\E[1;32m'
YELLOW='\E[1;33m'
NC='\033[1m\033[0m'

IFACE="$1"
IPSWITCH=${IPSWITCH:-192.168.127.253}
IPTHIS=${IPTHIS:-192.168.127.123}
MACNAME=${MACNAME:-Fastwel}

# Echo error string
echo_error() {
    echo -e "${RED}$*${NC}" 1>&2
}

# Echo warning string
echo_warn() {
    echo -e "${YELLOW}$*${NC}" 1>&2
}

# Echo info string
echo_info() {
    echo -e "${GREEN}$*${NC}"
}

main() {
    if sudo arp-scan -x -I "$IFACE" -s "$IPTHIS" "$IPSWITCH" | grep -qi "$MACNAME"; then
        echo_info Success
        return
    fi

    echo_error Failure
    exit 1
}

main
