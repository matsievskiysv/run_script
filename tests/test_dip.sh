#!/usr/bin/env bash

RED='\E[1;31m'
GREEN='\E[1;32m'
YELLOW='\E[1;33m'
NC='\033[1m\033[0m'

PORT=$(cat serial_port)
PORT=${PORT:-/dev/ttyUSB0}
BAUD=${BAUD:-115200}
USERNAME=${USERNAME:-admin}
PASSWORD=${PASSWORD:-password}
export TERM=vt220

# Echo error string
echo_error() {
    echo -e "${RED}$*${NC}" 1>&2
}

# Echo warning string
echo_warn() {
    echo -e "${YELLOW}$*${NC}" 1>&2
}

# Echo info string
echo_info() {
    echo -e "${GREEN}$*${NC}"
}

# Request user action and wait for confirm
# 1: title
# 2: text
user_inform() {
    if [ -z "$DISPLAY" ]; then
        echo_warn "$2"
        echo_warn "Press Enter to continue"
        read -r
    else
        zenity --title="$1" \
	       --text="$2" \
	       --info
    fi
}

# Check logic
# 1: state name
# 2: state values
check() {
    # user_inform "User action" "Turn all DIP switches $1"
    # shellcheck disable=SC2034
    for i in $(seq 5); do
        if dip.exp "${PORT}" "${BAUD}" "${USERNAME}" "${PASSWORD}" "$2"; then
	    echo_info Success
	    return
        fi
    done

    echo_error Failure
    exit 1
}

# Main
main() {
    check "UP (1)" "11111111"
    check "DOWN (0)" "00000000"
}

# main
check "" "$1"
