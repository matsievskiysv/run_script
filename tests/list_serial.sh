#!/usr/bin/env bash

find /dev -iname "ttyS*" -o -iname "ttyUSB*" | jq --slurp -c -R 'split("\n")|map(select(. != ""))'
