import { io } from "/static/socket.io.esm.min.js";
/* import { io } from "https://cdn.socket.io/4.4.1/socket.io.esm.min.js"; */

const socket = io();

socket.on("connect", () => {
    // request server status on startup
    socket.emit("status_request");
});

var archive_requested = false;
const scriptName = window.location.pathname.split("/").slice(-1)[0];

function enable_buttons(start, stop, clear, zip, download) {
    document.getElementById("start_script").disabled = !start;
    document.getElementById("stop_script").disabled = !stop;
    document.getElementById("clear_script").disabled = !clear;
    document.getElementById("zip_logs").disabled = !zip;
    document.getElementById("download_logs").disabled = !download;
}

function extract_value(element) {
    switch (element.tagName) {
	    case "INPUT":
		switch (element.type) {
			case "checkbox":
			    return String(element.checked);
			default:
			    return "";
		}
	    case "SELECT":
		let options = [];
		Array.from(element.selectedOptions).forEach((el) => {options.push(el.value);});
		return options.join(" ");
	    case "P":
		return element.textContent;
	    default:
		return "";
    }
}

function set_value(element, valuesArray) {
    switch (element.tagName) {
	    case "INPUT":
		switch (element.type) {
			case "checkbox":
			    element.checked = valuesArray.shift();
			    break;
			default:
			    break;
		}
		break;
	    case "SELECT":
		element.innerHTML = "";
		element.multiple = valuesArray.shift();
		const options = valuesArray.shift();
		options.forEach((option) => {
		    const opt = document.createElement("option");
		    opt.value = option;
		    opt.innerHTML = option;
		    element.appendChild(opt);
		});
		element.size = Math.min(options.length, 5);
		break;
	    case "SPAN":
		// fall through
	    case "P":
		element.textContent = valuesArray.shift();
		break;
	    default:
		break;
    }
}

function setup_input(config) {
    const element = document.getElementById(`stage_list_item_${config["position"]}`);
    const valueArray = config["payload"];
    element.querySelectorAll(".output_element").forEach((el) => {
	set_value(el, valueArray);
    });
}

function update_other(config) {
    switch (config["state"]) {
	    case "WAITING":
		enable_buttons(true, false, false, false, false);
		archive_requested = false;
		break;
	    default:
		enable_buttons(false, false, false, false, false);
		break;
    }
    const listElements = document.getElementsByClassName("stage_list_items");
    Array.from(listElements).forEach((item) => {
	const hiddenElements = item.querySelectorAll(".hidden_element");
	hiddenElements.forEach((el) => {
	    el.style.display = "none";
	});
	item.classList.remove("selected_item");
    });
}


function update_self(config) {
    switch (config["state"]) {
	    case "WAITING":
		enable_buttons(true, false, false, false, false);
		archive_requested = false;
		break;
	    case "RUNNING":
		enable_buttons(false, true, false, false, false);
		break;
	    case "AWAIT_INPUT":
		setup_input(config);
		enable_buttons(false, true, false, false, false);
		break;
	    case "AWAIT_CLEANUP":
		if (archive_requested) {
		    enable_buttons(false, false, true, false, true);
		} else {
		    enable_buttons(false, false, true, true, false);
		}
		break;
	    default:
		enable_buttons(false, false, false, false, false);
		break;
    }
    const listElements = document.getElementsByClassName("stage_list_items");
    for (let i = 0; i < listElements.length; i++) {
	const hiddenElements = listElements[i].querySelectorAll(".hidden_element");
	hiddenElements.forEach((el) => {
	    if (config["state"] == "RUNNING" || config["state"] == "AWAIT_INPUT") {
		el.style.display = i == config["position"] ? "initial" : "none";
	    } else {
		el.style.display = "none";
	    }
	});
	if (config.position == i) {
	    if (config["state"] == "RUNNING" || config["state"] == "AWAIT_INPUT") {
		listElements[i].classList.add("selected_item");
	    } else {
		listElements[i].classList.remove("selected_item");
	    }
	} else {
	    listElements[i].classList.remove("selected_item");
	}
    }
    const server_message = document.getElementById("server_message");
    server_message.textContent = config["message"];
}

function download_logs(scriptName) {
    fetch(`/scripts/${scriptName}/logs`).then(res => {
	if (res.ok) {
	    const aTag = document.createElement("a");
	    const date = new Date();
	    aTag.href = `/scripts/${scriptName}/logs`;
	    aTag.download = `${scriptName}-${date.toISOString()}.zip`;
	    document.body.appendChild(aTag);
	    aTag.click();
	    aTag.remove();
	}
    }).catch((e) => {
	// not yet available
    });
    document.getElementById("download_logs").textContent = "Download logs";
}

socket.on("status_update", (msg) => {
    const server_status = document.getElementById("server_status");
    let status = "INVALID";
    // console.log(msg);
    try {
	const message = JSON.parse(msg);
	status = message["state"];
	if (message["script"] == scriptName) {
	    update_self(message);
	} else {
	    update_other(message);
	}
    } catch (e) {
	console.log(e.message);
    }
    server_status.textContent = status;
});

socket.on("server_message", (msg) => {
    const server_message = document.getElementById("server_message");
    server_message.textContent = msg;
});

window.addEventListener("load", () => {
    document.getElementById("start_script").addEventListener("click", () => {
	socket.emit("start_script", scriptName, 0);
    });
    document.getElementById("stop_script").addEventListener("click", () => {
	socket.emit("stop_script", scriptName);
    });
    document.getElementById("clear_script").addEventListener("click", () => {
	socket.emit("cleanup_script", scriptName);
    });
    document.getElementById("zip_logs").addEventListener("click", () => {
	socket.emit("request_archive", scriptName);
	archive_requested = true;
    });
    document.getElementById("download_logs").addEventListener("click", () => {
	download_logs(scriptName);
    });
    Array.from(document.getElementsByClassName("confirm_input")).forEach((element) => {
	element.addEventListener("click", (event) => {
	    let elementIndex = parseInt(event.target.id.split("_").slice(-1)[0]);
	    const parent = document.getElementById(`stage_list_item_${elementIndex}`);
	    parent.querySelectorAll(".input_element").forEach((el) => {
		socket.emit("append_input", scriptName, extract_value(el));
	    });
	    socket.emit("continue_script", scriptName);
	});
    });
});
