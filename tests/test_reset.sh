#!/usr/bin/env bash

RED='\E[1;31m'
GREEN='\E[1;32m'
YELLOW='\E[1;33m'
NC='\033[1m\033[0m'

PORT=$(cat serial_port)
BAUD=${BAUD:-115200}
USERNAME=${USERNAME:-admin}
PASSWORD=${PASSWORD:-password}
export TERM=vt220

# Echo error string
echo_error() {
    echo -e "${RED}$*${NC}" 1>&2
}

# Echo warning string
echo_warn() {
    echo -e "${YELLOW}$*${NC}" 1>&2
}

# Echo info string
echo_info() {
    echo -e "${GREEN}$*${NC}"
}

# Request user action and wait for confirm
# 1: title
# 2: text
user_inform() {
    if [ -z "$DISPLAY" ]; then
        echo_warn "$2"
        echo_warn "Press Enter to continue"
        read -r
    else
        zenity --title="$1" \
	       --text="$2" \
	       --info
    fi
}

# Change prompt
# 1: change to
change_prompt() {
    # shellcheck disable=SC2034
    for i in $(seq 5); do
        if config_prompt.exp "${PORT}" "${BAUD}" "${USERNAME}" "${PASSWORD}" "${1}"; then
	    return
        fi
    done

    echo_error Failure
    exit 1
}

# Check logic
# 1: message
# 2: prompt
check() {
    # user_inform "User action" "$1"
    # shellcheck disable=SC2034
    # read -t 1 -r < "${PORT}"
    python3 -c "from serial import Serial; s=Serial('${PORT}', baudrate=${BAUD});s.read_all();s.close()"
    # # shellcheck disable=SC2034
    for i in $(seq 5); do
        if console.exp "${PORT}" "${BAUD}" "${USERNAME}" "${PASSWORD}" "$2"; then
	    echo_info Success
	    return
        fi
    done

    echo_error Failure
    exit 1
}

# Main
main() {
    change_prompt
    check "Press reset button and wait for switch to restart" "${PROMPT}"
}

case "$1" in
    change)
        change_prompt "$2"
        ;;
    check)
        check "" "$2"
        ;;
    *)
        echo_error 'Usage: ./test_reset.sh change|check'
        ;;
esac
