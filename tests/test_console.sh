#!/usr/bin/env bash

RED='\E[1;31m'
GREEN='\E[1;32m'
YELLOW='\E[1;33m'
NC='\033[1m\033[0m'

PORT="$1"
BAUD=${BAUD:-115200}
USERNAME=${USERNAME:-admin}
PASSWORD=${PASSWORD:-password}
PROMPT=${PROMPT:-NM800}
export TERM=vt220

# Echo error string
echo_error() {
    echo -e "${RED}$*${NC}" 1>&2
}

# Echo warning string
echo_warn() {
    echo -e "${YELLOW}$*${NC}" 1>&2
}

# Echo info string
echo_info() {
    echo -e "${GREEN}$*${NC}"
}

# Main
# $1: serial port
main() {
    # shellcheck disable=SC2034
    for i in $(seq 5); do
        if console.exp "${PORT}" "${BAUD}" "${USERNAME}" "${PASSWORD}" "${PROMPT}"; then
	    echo_info Success
	    return
        fi
    done

    echo_error Failure
    exit 1
}

main
