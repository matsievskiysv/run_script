FROM_STREAM_FILE = {
    "type": "object",
    "required": ["type", "index", "stream", "format"],
    "additionalProperties": False,
    "properties": {
        "type": {
            "const": "from_stream_file",
        },
        "index": {
            "type": "integer",
            "minimum": 0,
        },
        "stream": {
            "enum": ["output", "error"],
        },
        "format": {
            "enum": ["json"],
        },
    }
}

FROM_FILE = {
    "type": "object",
    "required": ["type", "name", "format"],
    "additionalProperties": False,
    "properties": {
        "type": {
            "const": "from_file",
        },
        "name": {
            "type": "string",
            "format": "uri",
        },
        "format": {
            "enum": ["json"],
        },
    }
}

SCRIPT_TITLE_ELEMENT = {
    "type": "string",
    "maxLength": 40,
}

SCRIPT_FILE = {
    "type": "object",
    "required": ["type", "title", "ref"],
    "additionalProperties": False,
    "properties": {
        "type": {
            "const": "file",
        },
        "title": SCRIPT_TITLE_ELEMENT,
        "ref": {
            "type": "string",
            "pattern": "^[\\d\\w]+$",
        },
        "stop_on_error": {
            "type": "boolean",
        },
        "success_codes": {
            "type": "array",
            "minItems": 1,
            "items": {
                "type": "integer",
            },
        },
        "extra_args": {
            "type": "array",
            "items": {
                "type": "string",
            },
        },
    }
}

SCRIPT_MESSAGE = {
    "type": "object",
    "required": ["type", "title", "text"],
    "additionalProperties": False,
    "properties": {
        "type": {
            "const": "message",
        },
        "title": SCRIPT_TITLE_ELEMENT,
        "text": {
            "anyOf": [
                FROM_FILE,
                FROM_STREAM_FILE,
                {
                    "type": "string",
                    "maxLength": 200,
                },
            ],
        },
    }
}

SCRIPT_TAIL = {
    "type": "object",
    "required": ["type", "title", "index", "count", "stream"],
    "additionalProperties": False,
    "properties": {
        "type": {
            "const": "tail",
        },
        "title": SCRIPT_TITLE_ELEMENT,
        "index": {
            "type": "integer",
            "minimum": 0,
        },
        "count": {
            "type": "integer",
            "minimum": 1,
        },
        "stream": {
            "enum": ["output", "error"],
        },
    }
}

SCRIPT_CHECKBOX = {
    "type": "object",
    "required": ["type", "title", "text"],
    "additionalProperties": False,
    "properties": {
        "type": {
            "const": "checkbox",
        },
        "title": SCRIPT_TITLE_ELEMENT,
        "text": {
            "anyOf": [
                FROM_FILE,
                FROM_STREAM_FILE,
                {
                    "type": "string",
                    "maxLength": 200,
                },
            ],
        },
        "value": {
            "anyOf": [
                FROM_FILE,
                FROM_STREAM_FILE,
                {
                    "type": "boolean",
                },
            ],
        }
    }
}

SCRIPT_LIST = {
    "type": "object",
    "required": ["type", "title", "text", "items"],
    "additionalProperties": False,
    "properties": {
        "type": {
            "const": "list",
        },
        "title": SCRIPT_TITLE_ELEMENT,
        "text": {
            "anyOf": [
                FROM_FILE,
                FROM_STREAM_FILE,
                {
                    "type": "string",
                    "maxLength": 200,
                },
            ],
        },
        "multiple": {
            "anyOf": [
                FROM_FILE,
                FROM_STREAM_FILE,
                {
                    "type": "boolean",
                },
            ],
        },
        "items": {
            "anyOf": [
                FROM_FILE,
                FROM_STREAM_FILE,
                {
                    "type": "array",
                    "minItems": 1,
                    "items": {
                        "type": "string",
                        "minLength": 1,
                        "maxLength": 50,
                    }
                },
            ],
        },
    }
}

LIST_SCHEMA = {
    "type": "object",
    "required": ["scripts", "environment", "files", "summary"],
    "properties": {
        "files": {
            "type": "object",
            "uniqueItems": True,
            "additionalProperties": {
                "type": "object",
                "required": ["file"],
                "uniqueItems": True,
                "additionalProperties": False,
                "properties": {
                    "file": {
                        "type": "string",
                        "format": "uri"
                    },
                }
            }
        },
        "environment": {
            "type": "object",
            "uniqueItems": True,
            "required": ["path"],
            "additionalProperties": False,
            "properties": {
                "path": {
                    "type": "array",
                    "items": {
                        "type": "string",
                        "format": "uri",
                    }
                },
            }
        },
        "summary": {
            "enum": ["return_code_check"],
        },
        "scripts": {
            "type": "object",
            "uniqueItems": True,
            "minProperties": 1,
            "additionalProperties": {
                "type": "object",
                "required": ["name", "stages"],
                "uniqueItems": True,
                "additionalProperties": False,
                "properties": {
                    "name": {
                        "type": "string",
                        "minLength": 3,
                        "maxLength": 50,
                    },
                    "desc": {
                        "type": "string",
                        "minLength": 3,
                        "maxLength": 200,
                    },
                    "stages": {
                        "type": "array",
                        "minItems": 1,
                        "items": {
                            "anyOf": [
                                SCRIPT_FILE,
                                SCRIPT_MESSAGE,
                                SCRIPT_TAIL,
                                SCRIPT_CHECKBOX,
                                SCRIPT_LIST,
                            ]
                        }
                    },
                }
            }
        }
    }
}
